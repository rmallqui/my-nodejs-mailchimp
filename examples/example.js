var Mailchimp = require('../index.js');
var crypto = require('crypto-js');

var api_key = 'ec3b7eb87acc5ad5817198e3f05c6fa6-us16'
var example_list_id = 'b5382037ec';

var mailchimp = new Mailchimp(api_key);

var callback = function (err, result) {
  console.log('callback');
  if (err) {
    console.log('error', err);
  }
  console.log(result);
  process.exit(0);
}

var status = {
  method : 'get',
  path : '/',
}

var get_lists = {
  method : 'get',
  path : '/lists',
}

var get_list_info = {
  method : 'get',
  path : '/lists/' + example_list_id
}

var get_list_info_path_params = {
  method : 'get',
  path : 'lists/{list_id}',
  path_params : {
    list_id : example_list_id
  }
}


// mailchimp.request(status, callback)
// mailchimp.request(get_lists, callback)
// mailchimp.request(get_list_info, callback)
// mailchimp.request(get_list_info_path_params, callback)


/*------------------------------------------------------------------------------------------
 Subscribir miembros   ------ POST ------
------------------------------------------------------------------------------------------*/
var post_list_menber = {
  method : 'post',
  path : 'lists/{list_id}/members',
  path_params : {
    list_id : example_list_id
  },
  body: {
      status        : 'subscribed',
      email_address : 'rmallqui07@continental.edu.pe'
    }
}

var post_list = {
  method : 'post',
  path : 'lists/{list_id}/members',
  path_params : {
    list_id : example_list_id
  }
}
var post_list_body = {
  status        : 'subscribed',
  email_address : 'rmallqui09@continental.edu.pe'
}
var post_members_list = [
    {
      method : 'POST',
      path   : 'lists/b5382037ec/members',
      body   : {
        email_address : 'rmallqui15@continental.edu.pe',
        status : 'subscribed'
      }
    },
    {
      method : 'POST',
      path   : 'lists/b5382037ec/members',
      body   : {
        email_address : 'rmallqui16@continental.edu.pe',
        status : 'subscribed'
      }
    }
]
var opt

//mailchimp.request(post_list_menber, callback)
//mailchimp.post(post_list,post_list_body, callback)
//mailchimp.batch(post_members_list,callback,opt)
/*------------------------------------------------------------------------------------------
 get info  ------ put ------
------------------------------------------------------------------------------------------*/
      // add columna
      var post_list = {
        method : 'post',
        path : 'lists/{list_id}/merge-fields',
        path_params : {
          list_id : example_list_id
        }
      }
      var merge_colum = {
        tag : 'LMAIL2', //-- max leng 9
        name : 'Last Mail',
        type : 'date'
      }
      //mailchimp.post(post_list,merge_colum, callback)


      // ---------------------- delete  colum-----------------------------
      var field_list = {
        path : 'lists/{list_id}/merge-fields/4',
        path_params : {
          list_id : example_list_id
        }
      }
      //mailchimp.delete(field_list,callback)

      // ---------------------- get info  colum-----------------------------
      var fields_list = {
        method : 'post',
        path : 'lists/{list_id}/merge-fields',
        path_params : {
          list_id : example_list_id
        }
      }
      var callbackfields = function (err, result) {
        console.log('callbackfields');
        if (err) {
          console.log('error', err);
        }
        var data = result.merge_fields; 
        for(var i = 0; i < data.length; i++) {
          console.log(data[i].tag + ' : ' + data[i].name + ' : ' +data[i].merge_id);
        }
        //console.log(result.merge_fields);
        process.exit(0);
      }
      //mailchimp.get(fields_list, callbackfields);



/*------------------------------------------------------------------------------------------
 set new member   ------ POST ------
------------------------------------------------------------------------------------------*/
      
      // agregar miembros
      var post_list = {
        method : 'post',
        path : 'lists/{list_id}/members',
        path_params : {
          list_id : example_list_id
        }
      }
      var post_list_body = {
        status        : 'subscribed',
        email_address : 'rmallqui17@continental.edu.pe'
      }
      //mailchimp.post(post_list,post_list_body, callback)

      //  insertar o actualziar mienbros de lista
      var put_members_list = [
          {
            method : 'PUT',
            path   : 'lists/b5382037ec/members/63fac67c97f4b3609ce5ed8357750a37',
            body   : {
              email_address : 'rmallqui01@continental.edu.pe',
              status : 'unsubscribed'
            }
          },
          {
            method : 'PUT',
            path   : 'lists/b5382037ec/members/' + crypto.MD5("rmallqui08@continental.edu.pe"),
            body   : {
              email_address : 'rmallqui08@continental.edu.pe',
              status : 'unsubscribed'
            }
          }
      ];
      //mailchimp.batch(put_members_list,callback,opt)

/*------------------------------------------------------------------------------------------
 --------------------------------get info campaigns  --------------------------------
------------------------------------------------------------------------------------------*/

      // ---------------------- get info  colum-----------------------------
      var campaigns_list = {
        method : 'get',
        path : '/campaigns'
      }
      var callbackcampaigns = function (err, result) {
        console.log('callbackfields');
        if (err) {
          console.log('error', err);
        }
        var data = result.campaigns; 
        for(var i = 0; i < data.length; i++) {
          console.log(data[i].tag + ' : ' + data[i].name + ' : ' +data[i].merge_id);
        }
        console.log(result);
        process.exit(0);
      }
      //mailchimp.get(campaigns_list, callbackcampaigns);


      // add columna
      var campaign = {
        method : 'post',
        path : '/campaigns'
      }
      var campaign_data = {
        type :'regular',
        recipients : {
          list_id : 'b5382037ec'
        },
        settings : {
          subject_line : 'Your Purchase Receipt',
          from_name : 'Customer Service',
          reply_to : 'onekyork@hotmail.com'
          
        }
      }
      //mailchimp.post(campaign,campaign_data, callback)


      // ---------------------- delete  colum-----------------------------
      var campaign_del = {
        method : 'delete',
        path : '/campaigns/4930727c04'
      }
      mailchimp.request(campaign_del,callback)



